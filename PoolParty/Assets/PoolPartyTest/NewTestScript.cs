using System.Collections.Generic;
using Assets.Scripts.Objects.BallLogic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.TestRunner;

public class NewTestScript
{

    // No Balls Struck --------------------------------------------------------------------------------

    [Test]
    public void Test_Unset_NoBallsStruck()
    {
        int currentPlayer = 0;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, -1);

        var fouls = FoulLogic.CalculateBallStruckFouls(-1, mapping, -1, currentPlayer);

        Assert.IsTrue(fouls[0].Equals("No balls struck"));
        Assert.IsTrue(fouls.Count == 1);
    }

    [Test]
    public void Test_Set_NoBallsStruck()
    {
        int currentPlayer = 0;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 1);

        var fouls = FoulLogic.CalculateBallStruckFouls(-1, mapping, 3, currentPlayer);

        Assert.IsTrue(fouls[0].Equals("No balls struck"));
        Assert.IsTrue(fouls.Count == 1);
    }


    // Correct Ball Struck --------------------------------------------------------------------------------


    [Test]
    public void Test_Unset_CorrectBallStruck()
    {
        int currentPlayer = 0;
        int secondPlayer = 1;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, -1);
        mapping.Add(secondPlayer, 0);
        mapping.Add(thirdPlayer, 1);

        var fouls = FoulLogic.CalculateBallStruckFouls(2, mapping, -1, currentPlayer);

        var errorString = StringListToTestOutput(fouls);

        Assert.IsTrue(fouls.Count == 0, errorString);
        
    }

    [Test]
    public void Test_Set_CorrectBallStruck()
    {
        int currentPlayer = 0;
        int secondPlayer = 1;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 2);
        mapping.Add(secondPlayer, 0);
        mapping.Add(thirdPlayer, 1);

        var fouls = FoulLogic.CalculateBallStruckFouls(2, mapping, 2, currentPlayer);

        var errorString = StringListToTestOutput(fouls);

        Assert.IsTrue(fouls.Count == 0, errorString);

    }


    // Black Ball Struck --------------------------------------------------------------------------------


    [Test]
    public void Test_Unset_BlackBallStruckEarly()
    {
        int currentPlayer = 0;
        int secondPlayer = 1;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, -1);
        mapping.Add(secondPlayer, 0);
        mapping.Add(thirdPlayer, 1);

        var fouls = FoulLogic.CalculateBallStruckFouls(50, mapping, -1, currentPlayer);

        var errorString = StringListToTestOutput(fouls);

        Assert.IsTrue(fouls[0].Equals("Black ball struck early"));
        Assert.IsTrue(fouls.Count == 1,errorString);

    }

    [Test]
    public void Test_Set_BlackBallStruckEarly()
    {
        int currentPlayer = 0;
        int secondPlayer = 1;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 3);
        mapping.Add(secondPlayer, 0);
        mapping.Add(thirdPlayer, 5);

        var fouls = FoulLogic.CalculateBallStruckFouls(50, mapping, 1, currentPlayer);

        var errorString = StringListToTestOutput(fouls);

        Assert.IsTrue(fouls[0].Equals("Incorrect colour struck - Black"), errorString);
        Assert.IsTrue(fouls.Count == 1, errorString);

    }


    // Correct Balls Pocketed --------------------------------------------------------------------------------


    [Test]

    public void Test_GoalBallPottedLegally()
    {
        int currentPlayer = 0;
        int secondPlayer = 1;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 2);
        mapping.Add(secondPlayer, 0);
        mapping.Add(thirdPlayer, 5);

        List<int> ballsLeft = new List<int>() { 2, 1, 0 }; // current player is index '2' mapping - no balls left

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(50, new List<int>() { 50 }, ballsLeft, mapping, currentPlayer);

        Assert.That(pockets.LegalPocket);
        Assert.That(pockets.GoalBallPocketed);
        Assert.That(pockets.ColourToSet == 99);
        Assert.That(pockets.fouls.Count == 0);
    }



    // Incorrect Balls Pocketed --------------------------------------------------------------------------------

    [Test]

    public void Test_Unset_OtherPlayersBallPocketed()
    {
        int currentPlayer = 2;
        int secondPlayer = 1;
        int thirdPlayer = 0;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, -1);
        mapping.Add(secondPlayer, 1);
        mapping.Add(thirdPlayer, 2);

        List<int> ballsLeft = new List<int>() { 2, 1, 0 }; // current player is unset mapping

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(0,new List<int>() { 1 }, ballsLeft, mapping, currentPlayer);

        Assert.That(!pockets.LegalPocket);
        Assert.That(pockets.ColourToSet == 99);
        Assert.That(pockets.fouls.Count == 1);
    }

    [Test]

    public void Test_Unset_OtherPlayersBallsPocketed()
    {
        int currentPlayer = 2;
        int secondPlayer = 1;
        int thirdPlayer = 0;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, -1);
        mapping.Add(secondPlayer, 1);
        mapping.Add(thirdPlayer, 2);

        List<int> ballsLeft = new List<int>() { 2, 1, 3 }; 

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(0,new List<int>() { 1 , 2 }, ballsLeft, mapping, currentPlayer);

        Assert.That(!pockets.LegalPocket);
        Assert.That(pockets.fouls.Count == 2);
    }

    [Test]

    public void Test_Set_OtherPlayersBallPocketed()
    {
        int currentPlayer = 2;
        int secondPlayer = 1;
        int thirdPlayer = 0;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 0);
        mapping.Add(secondPlayer, 1);
        mapping.Add(thirdPlayer, 2);

        List<int> ballsLeft = new List<int>() { 2, 1, 3 };

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(0, new List<int>() { 2 }, ballsLeft, mapping, currentPlayer);

        Assert.That(!pockets.LegalPocket);
        Assert.That(pockets.ColourToSet == 99);
        Assert.That(pockets.fouls.Count == 1);
    }

    [Test]

    public void Test_Set_OtherPlayersBallPocketedAfterLegalPocket()
    {
        int currentPlayer = 1;
        int secondPlayer = 0;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 2);
        mapping.Add(secondPlayer, 1);
        mapping.Add(thirdPlayer, 0);

        List<int> ballsLeft = new List<int>() { 2, 1, 3 };

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(2, new List<int>() { 2 , 0 }, ballsLeft, mapping, currentPlayer);

        var errorString = StringListToTestOutput(pockets.fouls);

        Assert.That(pockets.fouls.Count == 1, errorString);
    }

    [Test]

    public void Test_Set_BlackBallPocketedIllegallyAfterLegalPocket()
    {
        int currentPlayer = 1;
        int secondPlayer = 0;
        int thirdPlayer = 2;
        Dictionary<int, int> mapping = new Dictionary<int, int>();
        mapping.Add(currentPlayer, 2);
        mapping.Add(secondPlayer, 1);
        mapping.Add(thirdPlayer, 0);

        List<int> ballsLeft = new List<int>() { 2, 1, 3 };

        ShotProcessingInfo pockets = FoulLogic.ProcessShot(2,new List<int>() { 2, 50 }, ballsLeft, mapping, currentPlayer);

        var errorString = StringListToTestOutput(pockets.fouls);

        Assert.That(pockets.fouls.Count == 1, errorString);
    }











    // Helper Methods --------------------------------------------------------------------------------

    private string StringListToTestOutput(List<string> lines)
    {
        var output = "\n\r------------\n\r";
        foreach (string line in lines)
        {
            output += line;
        }
        output += "\n\r------------\n\r";

        return output;
    }
}
