using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Objects.BallLogic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class BallMappingTest
{
    [Test]
    public void Test_SetRemainingUnsetPlayerToCorrectColour()
    {
        // make sure that if there are two colours left, but the third is not referenced by BallMapping because that player is on the black
        // we still set the correct colour to the unset player

        Dictionary<int, int> mappings = new Dictionary<int, int>();
        List<int> ballsOnTable = new List<int>();

        mappings.Add(0, 50); // was set to '1'
        mappings.Add(1, -1); // should get set to '2'
        mappings.Add(2, 0); // has just potted a '0'

        ballsOnTable.Add(1); // player index 2
        ballsOnTable.Add(0); // player index 0 is on the black
        ballsOnTable.Add(2); // player index 1 should be assigned to ball index 2

        BallMapping.AutoUpdatePlayerBallMappings(mappings, ballsOnTable);


        Assert.That(mappings[1] == 2);
    }

}
