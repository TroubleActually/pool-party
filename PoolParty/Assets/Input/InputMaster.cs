// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""UI"",
            ""id"": ""fee1fda7-f89a-4500-ade2-23ef8b0dd8e8"",
            ""actions"": [
                {
                    ""name"": ""ProgressMenu"",
                    ""type"": ""Button"",
                    ""id"": ""ed395384-5c05-40c9-a879-c24b4c8f6e71"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectPlayer"",
                    ""type"": ""Button"",
                    ""id"": ""e40e84c5-fc7d-4df8-87f0-a3dc254631da"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""c4f3ad31-a4bf-4455-84b0-a27617f0160b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""eabed1a3-8b30-4c38-a37e-314df2344d31"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ProgressMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b065d4bc-a78c-473a-9efd-27e9f8af1880"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ProgressMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""06de4843-c2d2-4864-8c10-52a8bf9cf3e3"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ProgressMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""677bc3b4-25f9-45d7-b498-efa372b3613b"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectPlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""11942ad3-862e-4812-95bd-d7eb6c1c533f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectPlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3659a1cc-1ff6-4788-a9b5-8fabb5233892"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f1beb561-9830-4a8a-ad85-c2bbe84544ca"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Player"",
            ""id"": ""6681e68f-c4be-401c-9d2f-1a3ce5dd3b5a"",
            ""actions"": [
                {
                    ""name"": ""AimAxis"",
                    ""type"": ""Value"",
                    ""id"": ""3fba602f-b16d-4972-84dc-18eec7a077f7"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""17d943a9-5314-4ef3-be7d-74b8e2e6e612"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltView"",
                    ""type"": ""Button"",
                    ""id"": ""dd4bc076-6955-453f-9385-00aaba5ddbf2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AimButton"",
                    ""type"": ""Button"",
                    ""id"": ""460e32ce-7124-45b5-ae1c-2deba7c98c15"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SpinControl"",
                    ""type"": ""Value"",
                    ""id"": ""1f6fe685-0c35-4e1f-b753-0c1f476234c9"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AimHeightAxis"",
                    ""type"": ""Value"",
                    ""id"": ""19a78992-24f7-4afa-95c3-fcb1ef6e3152"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AimHeightButton"",
                    ""type"": ""Button"",
                    ""id"": ""20a14180-fec4-4702-8eb7-5d7ff9d998e6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FollowView"",
                    ""type"": ""Button"",
                    ""id"": ""318bddd0-65ec-4e3e-9794-1e067cf1a8bf"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4b41e655-88b9-4ef0-a14a-e12fcbe13ecb"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimAxis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41275c86-e70d-4053-836f-bbed32fbd79e"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f4615579-3aa6-4a9d-8fd2-a47dc7bc04c4"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8b18890f-879c-4bdc-8a3c-9e0591f87799"",
                    ""path"": ""<Mouse>/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47feb19d-4b8d-4cf0-844a-952d4b1f35fa"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AltView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0c4a784c-b79e-4607-a130-90ef0182cfec"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AltView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6c59ba08-b334-4383-8620-e5d85377a9e0"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AltView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a4c46f66-e57b-46fb-8820-95319ab7afd5"",
                    ""path"": ""<Gamepad>/leftStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85fa4535-3f01-41ac-ac13-884f236b7923"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""5dd3a145-2010-4e42-8f84-2435b7eda202"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""5997d987-4abe-467d-9be9-9711a3205eda"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""3258b651-f138-4a20-b7a8-ed5409fa1dec"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""3497cad2-7930-4d31-b359-81f7e258acef"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""4857f333-8fe7-479a-ac5a-abaef49360b4"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SpinControl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""91415629-a447-40d7-a640-2ee482d3632b"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimHeightAxis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""280fe5c1-4d99-4834-baf5-0b81e9ae5ed0"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimHeightButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""003420f0-cbf4-447b-8eba-b41c08296fcf"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FollowView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""51003f13-158f-4a27-a824-81bb942bdc41"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FollowView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""707bdc77-5ea3-4978-aaf1-7ca0e822c3d7"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FollowView"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_ProgressMenu = m_UI.FindAction("ProgressMenu", throwIfNotFound: true);
        m_UI_SelectPlayer = m_UI.FindAction("SelectPlayer", throwIfNotFound: true);
        m_UI_Back = m_UI.FindAction("Back", throwIfNotFound: true);
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_AimAxis = m_Player.FindAction("AimAxis", throwIfNotFound: true);
        m_Player_Fire = m_Player.FindAction("Fire", throwIfNotFound: true);
        m_Player_AltView = m_Player.FindAction("AltView", throwIfNotFound: true);
        m_Player_AimButton = m_Player.FindAction("AimButton", throwIfNotFound: true);
        m_Player_SpinControl = m_Player.FindAction("SpinControl", throwIfNotFound: true);
        m_Player_AimHeightAxis = m_Player.FindAction("AimHeightAxis", throwIfNotFound: true);
        m_Player_AimHeightButton = m_Player.FindAction("AimHeightButton", throwIfNotFound: true);
        m_Player_FollowView = m_Player.FindAction("FollowView", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_ProgressMenu;
    private readonly InputAction m_UI_SelectPlayer;
    private readonly InputAction m_UI_Back;
    public struct UIActions
    {
        private @InputMaster m_Wrapper;
        public UIActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @ProgressMenu => m_Wrapper.m_UI_ProgressMenu;
        public InputAction @SelectPlayer => m_Wrapper.m_UI_SelectPlayer;
        public InputAction @Back => m_Wrapper.m_UI_Back;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @ProgressMenu.started -= m_Wrapper.m_UIActionsCallbackInterface.OnProgressMenu;
                @ProgressMenu.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnProgressMenu;
                @ProgressMenu.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnProgressMenu;
                @SelectPlayer.started -= m_Wrapper.m_UIActionsCallbackInterface.OnSelectPlayer;
                @SelectPlayer.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnSelectPlayer;
                @SelectPlayer.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnSelectPlayer;
                @Back.started -= m_Wrapper.m_UIActionsCallbackInterface.OnBack;
                @Back.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnBack;
                @Back.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnBack;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ProgressMenu.started += instance.OnProgressMenu;
                @ProgressMenu.performed += instance.OnProgressMenu;
                @ProgressMenu.canceled += instance.OnProgressMenu;
                @SelectPlayer.started += instance.OnSelectPlayer;
                @SelectPlayer.performed += instance.OnSelectPlayer;
                @SelectPlayer.canceled += instance.OnSelectPlayer;
                @Back.started += instance.OnBack;
                @Back.performed += instance.OnBack;
                @Back.canceled += instance.OnBack;
            }
        }
    }
    public UIActions @UI => new UIActions(this);

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_AimAxis;
    private readonly InputAction m_Player_Fire;
    private readonly InputAction m_Player_AltView;
    private readonly InputAction m_Player_AimButton;
    private readonly InputAction m_Player_SpinControl;
    private readonly InputAction m_Player_AimHeightAxis;
    private readonly InputAction m_Player_AimHeightButton;
    private readonly InputAction m_Player_FollowView;
    public struct PlayerActions
    {
        private @InputMaster m_Wrapper;
        public PlayerActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @AimAxis => m_Wrapper.m_Player_AimAxis;
        public InputAction @Fire => m_Wrapper.m_Player_Fire;
        public InputAction @AltView => m_Wrapper.m_Player_AltView;
        public InputAction @AimButton => m_Wrapper.m_Player_AimButton;
        public InputAction @SpinControl => m_Wrapper.m_Player_SpinControl;
        public InputAction @AimHeightAxis => m_Wrapper.m_Player_AimHeightAxis;
        public InputAction @AimHeightButton => m_Wrapper.m_Player_AimHeightButton;
        public InputAction @FollowView => m_Wrapper.m_Player_FollowView;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @AimAxis.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimAxis;
                @AimAxis.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimAxis;
                @AimAxis.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimAxis;
                @Fire.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFire;
                @AltView.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAltView;
                @AltView.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAltView;
                @AltView.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAltView;
                @AimButton.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimButton;
                @AimButton.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimButton;
                @AimButton.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimButton;
                @SpinControl.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpinControl;
                @SpinControl.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpinControl;
                @SpinControl.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpinControl;
                @AimHeightAxis.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightAxis;
                @AimHeightAxis.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightAxis;
                @AimHeightAxis.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightAxis;
                @AimHeightButton.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightButton;
                @AimHeightButton.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightButton;
                @AimHeightButton.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAimHeightButton;
                @FollowView.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFollowView;
                @FollowView.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFollowView;
                @FollowView.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFollowView;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @AimAxis.started += instance.OnAimAxis;
                @AimAxis.performed += instance.OnAimAxis;
                @AimAxis.canceled += instance.OnAimAxis;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @AltView.started += instance.OnAltView;
                @AltView.performed += instance.OnAltView;
                @AltView.canceled += instance.OnAltView;
                @AimButton.started += instance.OnAimButton;
                @AimButton.performed += instance.OnAimButton;
                @AimButton.canceled += instance.OnAimButton;
                @SpinControl.started += instance.OnSpinControl;
                @SpinControl.performed += instance.OnSpinControl;
                @SpinControl.canceled += instance.OnSpinControl;
                @AimHeightAxis.started += instance.OnAimHeightAxis;
                @AimHeightAxis.performed += instance.OnAimHeightAxis;
                @AimHeightAxis.canceled += instance.OnAimHeightAxis;
                @AimHeightButton.started += instance.OnAimHeightButton;
                @AimHeightButton.performed += instance.OnAimHeightButton;
                @AimHeightButton.canceled += instance.OnAimHeightButton;
                @FollowView.started += instance.OnFollowView;
                @FollowView.performed += instance.OnFollowView;
                @FollowView.canceled += instance.OnFollowView;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    public interface IUIActions
    {
        void OnProgressMenu(InputAction.CallbackContext context);
        void OnSelectPlayer(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
    }
    public interface IPlayerActions
    {
        void OnAimAxis(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnAltView(InputAction.CallbackContext context);
        void OnAimButton(InputAction.CallbackContext context);
        void OnSpinControl(InputAction.CallbackContext context);
        void OnAimHeightAxis(InputAction.CallbackContext context);
        void OnAimHeightButton(InputAction.CallbackContext context);
        void OnFollowView(InputAction.CallbackContext context);
    }
}
