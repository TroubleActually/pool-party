using Assets.GameManagement;
using MLAPI;
using MLAPI.Messaging;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DeviceInput : NetworkBehaviour
{
    public static Action EnableActions;
    public static Action DisableActions;  

    public List<InputDevice> inputDevices;
    public List<InputDevice> mouseKeyboardDevices;

    private List<InputDevice> PendingInputDevices;
    public Dictionary<int,List<InputDevice>> mappedInputDevices;

    private List<string> ServerMappedInputDeviceNames;
    private List<string> ServerMappedInputDeviceTypeNames;
    private int NumMappedNetworkDevices;

    public int MaxNumPlayers;

    private string LocalPlayerName;

    [ClientRpc]
    void DisplayDevicesClientRpc(string devs)
    {
        DisplayInputDeviceMappings(devs);
    }

    [ClientRpc]
    void UpdateNumDevicesClientRpc(int devs)
    {
        NumMappedNetworkDevices = devs;
    }

    [ServerRpc(RequireOwnership = false)]
    void RequestAddDeviceServerRpc(string name, string devName, ulong client)
    {
        ConfirmAddDeviceClientRpc(NumMappedNetworkDevices, client);
        NewPlayerDeviceMapping(name, devName);  
    }

    [ClientRpc]
    void ConfirmAddDeviceClientRpc(int player, ulong client)
    {
        if (client == NetworkManager.Singleton.LocalClientId)
        {
            mappedInputDevices.Add(player, PendingInputDevices);
        }
    }

    private void Awake()
    {
        NetworkGUI.SetPlayerName += NetworkGUI_SetPlayerName;

        inputDevices = new List<InputDevice>();
        mouseKeyboardDevices = new List<InputDevice>();
        mappedInputDevices = new Dictionary<int,List<InputDevice>>();
        PendingInputDevices = new List<InputDevice>();
        NumMappedNetworkDevices = 0;

        inputDevices.AddRange(InputSystem.devices);

        foreach (InputDevice inDev in inputDevices)
        {
            if (inDev.name.Equals("Mouse") || inDev.name.Equals("Keyboard"))
            {
                mouseKeyboardDevices.Add(inDev);
            }
        } 
    }

    private void NetworkGUI_SetPlayerName(string name)
    {
        LocalPlayerName = name;
    }

    public override void NetworkStart()
    {
        InputManager.AddPlayerDevice += AddDeviceMapping;

        if (IsHost)
        {
            ServerMappedInputDeviceNames = new List<string>();
            ServerMappedInputDeviceTypeNames = new List<string>();
        }
    }

    [ClientRpc]
    public void LockMappingClientRpc()
    {
        InputManager.AddPlayerDevice -= AddDeviceMapping;
    }

    [ClientRpc]
    public void UnlockMappingClientRpc()
    {
        InputManager.AddPlayerDevice += AddDeviceMapping;
    }

    private void NewPlayerDeviceMapping(string name, string devName)
    {
        ServerMappedInputDeviceNames.Add(GenerateUniqueName(name));
        ServerMappedInputDeviceTypeNames.Add( devName );
        UpdateNumDevicesClientRpc(ServerMappedInputDeviceNames.Count);
        NumMappedNetworkDevices = ServerMappedInputDeviceNames.Count;
        DisplayInputDeviceMappings(GenerateDeviceDisplay());
        DisplayDevicesClientRpc(GenerateDeviceDisplay());      
    }

    private void AddDeviceMapping(InputDevice dev)
    {
        if (NumMappedNetworkDevices < MaxNumPlayers)
        {
            PendingInputDevices = new List<InputDevice>();
            string devName;

            if (dev.name.Equals("Mouse") || dev.name.Equals("Keyboard"))
            {
                PendingInputDevices.AddRange(mouseKeyboardDevices);
                devName = "Mouse/Keyboard";
            }
            else
            {
                PendingInputDevices.Add(dev);
                devName = "Gamepad";
            }

            if (IsHost)
            {
                mappedInputDevices.Add(NumMappedNetworkDevices, PendingInputDevices);
                NewPlayerDeviceMapping(LocalPlayerName,devName);
            }
            else
            {
                RequestAddDeviceServerRpc(LocalPlayerName,devName, NetworkManager.Singleton.LocalClientId);
            }
        }
    }


    public int GetNumMappedDevices()
    {
        return NumMappedNetworkDevices;
    }

    public void SwitchDevice(int player)
    {
        if (IsHost)
        {
            SwitchDeviceClientRpc(player);
        }
    }

    [ClientRpc]
    void SwitchDeviceClientRpc(int player)
    {
        SwitchLocalDevice(player);
    }

    public void SwitchLocalDevice(int player)
    {
        DisableActions?.Invoke();

        if (mappedInputDevices.ContainsKey(player))
        {
            EnableActions?.Invoke();
        }   

    }

    public int SwitchToAllDevices()
    {
        EnableActions?.Invoke();

        mappedInputDevices.Clear();

        if (IsHost)
        {
            ServerMappedInputDeviceNames.Clear();
            NumMappedNetworkDevices = 0;
            UpdateNumDevicesClientRpc(0);
        }

        return inputDevices.Count;
    }

    private string GenerateDeviceDisplay()
    {
        string text = "";

        for (int i = 0; i < ServerMappedInputDeviceNames.Count; i++)
        {
            if (ServerMappedInputDeviceNames[i] == "")
            {
                text += "Player " + (i + 1) + ": " + ServerMappedInputDeviceTypeNames[i] + "\n\r";
            }
            else
            {
                text += ServerMappedInputDeviceNames[i] + ": " + ServerMappedInputDeviceTypeNames[i] + "\n\r";
            }
        }

        return text;
    }

    private void DisplayInputDeviceMappings(string display)
    {
        GameManager.DisplayText?.Invoke(display, 12);
    }

    public string GetPlayerName(int player)
    {
        return ServerMappedInputDeviceNames[player];
    }

    private string GenerateUniqueName(string name)
    {
        string newName = name;
        if (ServerMappedInputDeviceNames.Contains(name))
        {
            for (int i=1;i<MaxNumPlayers;i++)
            {
                newName = name + i;
                if (!ServerMappedInputDeviceNames.Contains(newName))
                    break;
            }
      
        }
        return newName;
    }
}
