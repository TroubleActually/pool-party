﻿using Assets.GameManagement;
using Assets.Scripts.Objects;
using UnityEngine;

public class FollowCueBallObject : MonoBehaviour
{
    private Transform cueBallTransform;
    private Transform playerTransform;

    private void Awake()
    {
        GameManager.StartGame += FindObjects;
    }

    private void FindObjects(int p)
    {
        cueBallTransform = FindObjectOfType<CueBall>().transform;
        playerTransform = FindObjectOfType<InputManager>().transform;
    }
   
    private void Update()
    {
        if (cueBallTransform)
        {
            transform.position = cueBallTransform.position;
            transform.rotation = playerTransform.rotation;
        }
    }

}