using Assets.Scripts.Objects;
using MLAPI;
using MLAPI.Messaging;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.GameManagement
{
    public class GameManager : NetworkBehaviour
    {
        // Events
        public static event Action<int> StartGame;
        public static Action<Vector3, Vector3, int> TurnStarted;
        public static Action<string, int> DisplayText;
        public static Action<string, int, int> DisplayTextForTurns;
        public static Action<DeviceInput> RegisterDeviceInput;

        public Camera CueCamera;
        public Camera OHCamera;
        public Camera BallCamera;
        public Camera LobbyCamera;

        public Rules GameRules;
        private int NumPlayers;
        private List<int> remainingPlayers;

        private int currentPlayer;
        private int currentPlayerShots = 1;
        private bool lobby;
        public BallHandler BallHandler;
        private DeviceInput DeviceInput;


        // RPC methods

        [ClientRpc]
        void SwitchToLobbyClientRPC(string message)
        {
            SwitchToLobby(message);
        }

        [ClientRpc]
        void SwitchFromLobbyClientRPC()
        {
            // RPC is slow and breaks Host logic, so this is called directly where the RPC gets called
            if (!IsHost)
            {
                SwitchFromLobby();
            }
        }

        [ClientRpc]
        void DisplayPlayersClientRPC(string upper, string lower)
        {
            DisplayText?.Invoke(upper, 2);
            DisplayText?.Invoke(lower, 4);
        }

        [ClientRpc]
        void DisplayFoulTextClientRpc(string text)
        {
            DisplayTextForTurns?.Invoke(text, 2, 1);
        }

        // Unity messages 

        private void Awake()
        {
            BallHandler.ShotComplete += OnShotComplete;
            InputManager.OnAltCamera += AlternateCamera;
            InputManager.OnAltCameraCancel += MainCamera;
            InputManager.Skip += BeginRound;
            InputManager.Back += OnBackSelected;
        }

        private void Start()
        {
            DeviceInput = GetComponent<DeviceInput>();

            InitialisePlayers(UnityEngine.Random.Range(1, 7));

            BallHandler.BeginSession(GameRules, NumPlayers);
            
            SwitchToLobby("Pool Party");

            // Display controls
            DisplayText?.Invoke("Add players: F / SouthButton\n\rStart: LCtrl / StartButton\n\rQuit: Q / SelectButton", 11);

        }

        public override void NetworkStart()
        {
            if (!IsHost)
            {
                BallHandler.ClearDown();
            }
        }

        private void InitialisePlayers(int count)
        {
            NumPlayers = count;
            remainingPlayers = new List<int>();
            for (int i=0; i<NumPlayers; i++)
            {
                remainingPlayers.Add(i);
            }
        }

        private void OnBackSelected()
        {
            if (!lobby)
            {
                SwitchToLobby("Pool Party");

                if (IsHost)
                {
                    SwitchToLobbyClientRPC("Pool Party");
                }
            }
            else
            {
                Application.Quit();
            }
        }

        private void AlternateCamera(int alternative)
        {
            if (!lobby)
            {
                CueCamera.enabled = false;
                OHCamera.enabled = false;
                BallCamera.enabled = false;

                if (alternative == 0)
                    OHCamera.enabled = true;
                else
                    BallCamera.enabled = true;
            }
        }

        private void MainCamera()
        {
            if (!lobby)
            {
                OHCamera.enabled = false;
                BallCamera.enabled = false;
                CueCamera.enabled = true;
            }
        }

        private void StartTurn()
        {
            DeviceInput.SwitchDevice(currentPlayer);
            StartTurnClientRpc(new TurnInfo(BallHandler.CueBallCurrentPosition(), BallHandler.AverageBallPosition(currentPlayer),currentPlayer));
            DisplayCurrentPlayer(currentPlayer);
        }


        [ClientRpc]
        void StartTurnClientRpc(TurnInfo info)
        {
            TurnStarted?.Invoke(info.CueBallPosition, info.AverageBallPosition, info.PlayerId);           
        }

        private void DisplayCurrentPlayer(int current)
        {
            string currentName = PlayerName(current);
            string otherPlayers = "";

            if (remainingPlayers.Count > 1)
            {
                int player = NextPlayer(current);

                otherPlayers = PlayerName(player);

                for (int i = 0; i < remainingPlayers.Count - 2; i++)
                {
                    player = NextPlayer(player);
                    otherPlayers += "\n\r" + PlayerName(player);
                }

            }

            DisplayPlayersClientRPC(currentName, otherPlayers);
        }

        private void OnShotComplete(List<string> fouls, bool potted, bool goalBallPot)
        {
            if (!IsHost) { return; }

            currentPlayerShots -= 1;

            if (potted)
            {
                currentPlayerShots = 1;
            }

            if (fouls.Count != 0)
            {
                string foulText = PlayerName(currentPlayer) + " Foul:";

                foreach (string foul in fouls)
                {
                    foulText += "\n\r" + foul;
                }

                //DisplayTextForTurns(foulText, 2, 1);
                DisplayFoulTextClientRpc(foulText);
            }

            // END MATCH
            if (goalBallPot)
            {
                if (fouls.Count == 0)
                {
                    SwitchToLobbyClientRPC(PlayerName(currentPlayer) + " won the game!");   
                    return;
                }

                //  else disqualify the current player
                remainingPlayers.Remove(currentPlayer);
                //DisplayTextForTurns(PlayerName(currentPlayer) + " was disqualified", 2, 1);

                string foulText = " Foul:";

                foreach (string foul in fouls)
                {
                    foulText += "\n\r" + foul;
                }


                DisplayFoulTextClientRpc(PlayerName(currentPlayer) + " was disqualified" + foulText);

                if (remainingPlayers.Count < 2)
                {
                    if (remainingPlayers.Count == 1)
                    {
                        SwitchToLobbyClientRPC(PlayerName(remainingPlayers[0]) + " won the game!");
                    }
                    else
                    {
                        SwitchToLobby("Nobody won the game!");
                    }

                    return;
                }

            }

            // if current player has no shots left or fouled or was disqualified
            if (currentPlayerShots == 0 || fouls.Count != 0 || !remainingPlayers.Contains(currentPlayer))
            {
                currentPlayer = NextPlayer(currentPlayer);
                currentPlayerShots = 1;
                if (BallHandler.PlayerCanHaveExtraShot(currentPlayer) && fouls.Count != 0)
                {
                    currentPlayerShots = 2;
                }
            }

            StartTurn();

        }

        private void BeginRound()
        {
            if (IsHost)
            {
                if (lobby && DeviceInput.GetNumMappedDevices() > 0)
                {

                    SwitchFromLobby();
                    SwitchFromLobbyClientRPC();
                    DeviceInput.LockMappingClientRpc();

                    InitialisePlayers(DeviceInput.GetNumMappedDevices());
                    BallHandler.BeginSession(GameRules, NumPlayers);
                    currentPlayer = UnityEngine.Random.Range(0, NumPlayers);
                    StartGame?.Invoke(currentPlayer);
                    currentPlayerShots = 1;
                    DisplayText?.Invoke("Aim: Mouse/A-D/LStick\n\rPower: Hold LClick/F/SouthButton\n\rOH Camera: RClick/LShift/NorthButton\n\rQuit: Q/Select", 3);
                    StartTurn();

                }
            }
        }

        private void SwitchToLobby(string mainDisplayText)
        {
            if (!lobby)
            {
                lobby = true;
                CueCamera.enabled = false;
                OHCamera.enabled = false;
                BallCamera.enabled = false;
                LobbyCamera.enabled = true;
                DeviceInput.SwitchToAllDevices();
                DeviceInput.UnlockMappingClientRpc();
                DisplayText?.Invoke(mainDisplayText, 10);
                DisplayText?.Invoke("", 12);

                if (IsHost)
                {
                    BallHandler.EndGame();
                }
            }
        }

        private void SwitchFromLobby()
        {
            if (lobby)
            {
                lobby = false;
                LobbyCamera.enabled = false;
                CueCamera.enabled = true;
                //Debug.Log("Switching from Lobby" + IsHost);
            }
        }

        private int NextPlayer(int player)
        {
            int nextPlayer = player + 1;

            while (nextPlayer < NumPlayers)
            {               
                if (remainingPlayers.Contains(nextPlayer))
                {
                    return nextPlayer;
                }
                nextPlayer += 1;
            }

            return remainingPlayers[0];
        }

        private string PlayerName(int player)
        {
            string name = DeviceInput.GetPlayerName(player);
            if (name == "")
                name = "Player " + (player + 1);

            string colour;

            if (BallHandler.PlayerColour(player).Equals("Unset"))
            {
                name += ": Unset";
                colour = "#E0E0E0";
            }
            else
            {
                colour = BallHandler.PlayerColour(player);
            }

            return "<color=" + colour + "> " + name + " </color>";


        }
    }
}
