﻿using Assets.GameManagement;
using MLAPI;
using MLAPI.Messaging;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : NetworkBehaviour
{
    private InputMaster Controls;

    // Input things
    public float aimSpeed;
    public float discreteAimMultiplier;
    public float heightAimSpeed;
    public float aimSpinSpeed;

    private TransformUtil cue;
    private CueTip cueTip;
    private ShotAimPosition shotAimPosition;
    private CueHitPoint cueHitPoint;
    private CueCameraObject cueCamera;

    private int layerMask;

    private Vector3 spinRayDirection;
    //private Vector3 aimRayDirection; // used to draw raycast for aim
    private Vector3 yNormedCue;
    private Vector3 yNormedCueTip;
    //private Vector3 yNormedShotOrigin; // used to draw raycast for aim
    //private Vector3 yNormedBallPos; // used to draw raycast for aim
    private Vector3 hitPointStartPos;

    private Vector3 spinOffset;
    private Vector3 spinDir;
    private Vector3 spinAxis;

    private Vector3 localXRotation;

    private bool aiming;
    private float aimDirModifier;
    private bool adjustingCameraHeight;
    private float cameraHeightModifier;

    private bool aimingSpin;
    private Vector3 aimSpinChange;

    private RaycastHit hit;

    private int shotPower;
    private bool shotPowerHeld;
    private bool shotTaken;

    private bool CurrentPlayerTurn;


    // Events    
    public static event Action OnShotHeld;
    public static event Action<ShotInfo> OnShotReleased;
    public static event Action Skip;
    public static event Action Back;
    public static event Action<InputDevice> AddPlayerDevice;
    public static event Action<int> OnAltCamera;
    public static event Action OnAltCameraCancel;

    private void Awake()
    {
        CurrentPlayerTurn = true;
        DeviceInput.EnableActions += Actions;
        DeviceInput.DisableActions += NoActions;

        Controls = new InputMaster();
        Controls.UI.ProgressMenu.performed += _ => MenuProgress();
        Controls.UI.Back.performed += _ => OnExit();
        Controls.UI.SelectPlayer.performed += ctx => AddPlayer(ctx.control.device);
        Controls.Player.AimHeightAxis.performed += ctx => AimHeightContinuous(ctx.ReadValue<float>());
        Controls.Player.AimHeightButton.performed += ctx => AimHeightDiscrete(ctx.ReadValue<float>());
        Controls.Player.AimHeightButton.canceled += _ => AimHeightDiscreteEnd();
        Controls.Player.AimAxis.performed += ctx => AimContinuous(ctx.ReadValue<float>());
        Controls.Player.AimButton.performed += ctx => AimDiscrete(ctx.ReadValue<float>());
        Controls.Player.AimButton.canceled += _ => AimDiscreteEnd();
        Controls.Player.SpinControl.performed += ctx => AimSpinStart(ctx.ReadValue<Vector2>());
        Controls.Player.SpinControl.canceled += _ => AimSpinEnd();
        Controls.Player.Fire.performed += _ => ShotPress();
        Controls.Player.Fire.canceled += ctx => ShotRelease();
        Controls.Player.AltView.performed += _ => AltCamera();
        Controls.Player.AltView.canceled += _ => AltCameraCancel();
        Controls.Player.FollowView.performed += _ => BallCamera();
        Controls.Player.FollowView.canceled += _ => BallCameraCancel();

        layerMask = 1 << 8;

        Cursor.lockState = CursorLockMode.Confined;

    }

    private void Start()
    {
        GameManager.TurnStarted += BeginTurn;
        cue = GetComponentInChildren<TransformUtil>();
        cueTip = GetComponentInChildren<CueTip>();
        shotAimPosition = GetComponentInChildren<ShotAimPosition>();
        cueHitPoint = GetComponentInChildren<CueHitPoint>();
        hitPointStartPos = cueHitPoint.transform.position;
        cueCamera = GetComponentInChildren<CueCameraObject>();

        localXRotation = new Vector3(0,0,0);
    }

    private void OnEnable()
    {
        Controls.Enable();
    }

    [ServerRpc (RequireOwnership = false)] 
    void CurrentPlayerViewServerRpc(PlayerViewInfo playerViewInfo)
    {
        CurrentPlayerViewClientRpc(playerViewInfo);
    }

    [ClientRpc]
    void CurrentPlayerViewClientRpc(PlayerViewInfo playerViewInfo)
    {
        if (!CurrentPlayerTurn)
        {
            transform.rotation = playerViewInfo.CameraRotation;
            cueHitPoint.transform.position = playerViewInfo.CueHitPointerPosition;
        }
    }

    private void Update()
    {
        Vector3 ballCenter;

        localXRotation.x = transform.localPosition.x;

        // spin raycast
        yNormedCue = new Vector3(cue.transform.position.x, cueTip.transform.position.y - 0.26f, cue.transform.position.z);
        yNormedCueTip = new Vector3(cueTip.transform.position.x, cueTip.transform.position.y - 0.26f, cueTip.transform.position.z);

        // aim raycast
        //yNormedShotOrigin = new Vector3(shotAimPosition.transform.position.x, transform.position.y + 0.2f, shotAimPosition.transform.position.z);
        //yNormedBallPos = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);

        if (aiming)
        {
            transform.Rotate(Vector3.down, discreteAimMultiplier * aimDirModifier * aimSpeed);
        }

        if (adjustingCameraHeight)
        {
            // Controller seems to input up/down lower magnitude than left/right
            float adjustment = cameraHeightModifier * discreteAimMultiplier * 3;
            RotateCameraHeight(adjustment);
        }

        //aimRayDirection = yNormedBallPos - yNormedShotOrigin;
        spinRayDirection = yNormedCueTip - yNormedCue;
        //Debug.DrawRay(yNormedShotOrigin, aimRayDirection * 2, Color.cyan, Time.deltaTime);
        //Debug.DrawRay(yNormedCue, spinRayDirection * 2, Color.red, Time.deltaTime);

        if (aimingSpin)
        {
            Vector3 localSpinChange = (cue.transform.forward * aimSpinChange.x) + (cue.transform.right * aimSpinChange.y);
            Vector3 newPosition = cue.transform.position + localSpinChange;            
            
            // Clamp the cue to move within a circle slightly smaller than the ball size (radius 0.3)
            spinOffset = newPosition - shotAimPosition.transform.position;
            cue.transform.position = shotAimPosition.transform.position + Vector3.ClampMagnitude(spinOffset,0.25f);
        }

        // Make the aim sphere appear on the cue ball
        // and get the spin axis
        if (CurrentPlayerTurn)
        { 
            if (Physics.Raycast(yNormedCue, spinRayDirection * 5, out hit, 25, layerMask))
            {
                cueHitPoint.transform.position = hit.point;
                ballCenter = hit.transform.position;
                spinDir = ballCenter - hit.point;
                spinAxis = Vector3.Cross(spinRayDirection, spinDir);
            }
            else
            {
                cueHitPoint.transform.position = hitPointStartPos;
            }

            if (IsClient && !shotTaken)
            {
                CurrentPlayerViewServerRpc(new PlayerViewInfo(transform.rotation, cueHitPoint.transform.position));
            }
        }

        if (shotPowerHeld && shotPower < 100)
        {
            shotPower += 1;
        }

        if (shotTaken)
        {
            cueHitPoint.transform.position = hitPointStartPos;
        }
    }

    public void NoActions()
    {
        CurrentPlayerTurn = false;
        if (cueHitPoint)
            cueHitPoint.SetColour(false); 
    }

    public void Actions()
    {
        CurrentPlayerTurn = true;
        if (cueHitPoint)
            cueHitPoint.SetColour(true);
    }

    private void MenuProgress()
    {
        Skip?.Invoke();
    }

    private void OnExit()
    {
        Back?.Invoke();
    }

    private void AddPlayer(InputDevice dev)
    {
        if (CurrentPlayerTurn)
            AddPlayerDevice?.Invoke(dev);      
    }

    private void AimContinuous(float f)
    {
        float horz = f;
        if (CurrentPlayerTurn || shotTaken)
            transform.Rotate(Vector3.down, horz * aimSpeed);
    }

    private void AimDiscrete(float f)
    {
        if (CurrentPlayerTurn || shotTaken)
        {
            aiming = true;
            aimDirModifier = f;
        }
    }

    private void AimDiscreteEnd()
    {
        if (CurrentPlayerTurn || shotTaken)
            aiming = false;
    }

    private void AimHeightContinuous(float f)
    {
        RotateCameraHeight(f);
    }

    private void AimHeightDiscrete(float f)
    {
        adjustingCameraHeight = true;
        cameraHeightModifier = f;
    }

    private void AimHeightDiscreteEnd()
    {
        adjustingCameraHeight = false;
    }

    private void AimSpinStart(Vector2 dir)
    {
        aimingSpin = true;
        aimSpinChange = new Vector3 (dir.x,dir.y,0) * aimSpinSpeed;
    }

    private void AimSpinEnd()
    {
        aimingSpin = false;
    }

    private void ShotPress()
    {
        if (CurrentPlayerTurn)
        {
            OnShotHeld?.Invoke();
            shotPower = 0;
            shotPowerHeld = true;
        }       
    }

    [ServerRpc (RequireOwnership = false)]
    void ShotTakenServerRpc()
    {
        ShotTakenClientRpc();
    }

    [ClientRpc]
    void ShotTakenClientRpc()
    {
        shotTaken = true;
        cue.transform.position = cue.transform.position = hitPointStartPos;
    }

    private void ShotRelease()
    {
        if (CurrentPlayerTurn)
        {
            Vector3 direction = new Vector3(shotAimPosition.transform.position.x, 0, shotAimPosition.transform.position.z);
            shotPowerHeld = false;
            ShotInfo shot = new ShotInfo(direction, spinAxis, shotPower);
            OnShotReleased?.Invoke(shot);
            ShotTakenServerRpc();
        }
    }

    private void AltCamera()
    {
        OnAltCamera?.Invoke(0);
    }
    private void AltCameraCancel()
    {
        OnAltCameraCancel?.Invoke();
    }

    private void BallCamera()
    {
        OnAltCamera?.Invoke(1);
    }

    private void BallCameraCancel()
    {
        OnAltCameraCancel?.Invoke();
    }

    private void BeginTurn(Vector3 ballPosition, Vector3 lookTarget, int player)
    {
        aiming = false;
        shotTaken = false;
        AltCameraCancel();
        transform.position = ballPosition; 
        LookAtTarget(lookTarget);

        if (CurrentPlayerTurn)
        {
            cue.transform.position = shotAimPosition.transform.position;
        }
        else
        {
            cue.transform.position = hitPointStartPos;
        }
    }

    private void LookAtTarget(Vector3 target)
    {
        Vector3 lookPosition = new Vector3(target.x, transform.position.y, target.z);
        transform.LookAt(lookPosition);
    }

    private void RotateCameraHeight(float amount)
    {
        //  invert Y axis
        float xRotation = -amount * heightAimSpeed;

        float clampedRotation;

        // Clamp top to 0.4 bottom to -0.25 
        if ((cueCamera.transform.rotation.x + xRotation) > 0.4)
        {
            clampedRotation = 0.4f - cueCamera.transform.rotation.x;
            cueCamera.transform.Rotate(localXRotation, clampedRotation);
        }
        else if ((cueCamera.transform.rotation.x + xRotation) < -0.25)
        {
            clampedRotation = -0.25f - cueCamera.transform.rotation.x;
            cueCamera.transform.Rotate(localXRotation, clampedRotation);
        }
        else
        {
            cueCamera.transform.Rotate(localXRotation, xRotation);
        }
    }
}
