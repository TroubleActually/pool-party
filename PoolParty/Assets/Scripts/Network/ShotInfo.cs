using MLAPI.Serialization;
using UnityEngine;

public struct ShotInfo : INetworkSerializable
{
    public Vector3 Direction;
    public Vector3 Spin;
    public float ForceScale;

    public ShotInfo(Vector3 dir, Vector3 spin, float force)
    {
        Direction = dir;
        Spin = spin;
        ForceScale = force;
    }

    public void NetworkSerialize(NetworkSerializer serializer)
    {
        serializer.Serialize(ref Direction);
        serializer.Serialize(ref Spin);
        serializer.Serialize(ref ForceScale);
    }
}
