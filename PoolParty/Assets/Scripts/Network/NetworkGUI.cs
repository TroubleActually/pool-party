
using MLAPI;
using MLAPI.Transports.UNET;
using System;
using UnityEngine;

public class NetworkGUI : MonoBehaviour
{
    string playerName = "";
    string ipAddress = "";

    public static event Action<string> SetPlayerName;

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 200, 300));
        if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
        {
            StartButtons();
        }
        else
        {
            StatusLabels();
        }

        GUILayout.EndArea();
    }

    void StartButtons()
    {
        GUILayout.Label("Name");
        playerName = GUILayout.TextField(playerName,20);

        ipAddress = "86.25.131.60";

        if (GUILayout.Button("Host / Offline")) 
            BeginServer();
        if (GUILayout.Button("Client")) 
            BeginClient();

        //GUILayout.Label("IP Address");
        //ipAddress = GUILayout.TextField(ipAddress, 15);
        
    }

    void StatusLabels()
    {
        var mode = NetworkManager.Singleton.IsHost ?
            "Host" : NetworkManager.Singleton.IsServer ? "Server" : "Client";

        GUILayout.Label(mode + ": " + playerName);
    }

    void BeginClient()
    {
        if (ipAddress != "")
        {
            SetPlayerName(playerName);
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = ipAddress;
            NetworkManager.Singleton.StartClient();
        }
    }

    void BeginServer()
    {
        SetPlayerName(playerName);
        NetworkManager.Singleton.StartHost();
    }
}
