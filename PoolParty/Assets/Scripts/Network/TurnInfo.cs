using MLAPI.Serialization;
using UnityEngine;

public struct TurnInfo : INetworkSerializable
{
    public Vector3 CueBallPosition;
    public Vector3 AverageBallPosition;
    public int PlayerId;

    public TurnInfo(Vector3 cuePos, Vector3 avgPos, int player)
    {
        CueBallPosition = cuePos;
        AverageBallPosition = avgPos;
        PlayerId = player;
    }

    public void NetworkSerialize(NetworkSerializer serializer)
    {
        serializer.Serialize(ref CueBallPosition);
        serializer.Serialize(ref AverageBallPosition);
        serializer.Serialize(ref PlayerId);
    }
}
