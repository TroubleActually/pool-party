﻿using MLAPI.Serialization;
using UnityEngine;

public struct PlayerViewInfo : INetworkSerializable
{
    public Quaternion CameraRotation;
    public Vector3 CueHitPointerPosition;

    public PlayerViewInfo(Quaternion aim, Vector3 cueHitPos)
    {
        CameraRotation = aim;
        CueHitPointerPosition = cueHitPos;
    }

    public void NetworkSerialize(NetworkSerializer serializer)
    {
        serializer.Serialize(ref CameraRotation);
        serializer.Serialize(ref CueHitPointerPosition);
    }
}