using UnityEngine;

public class TransformUtil : MonoBehaviour
{
    private Transform _transform;
    public Transform Transform => _transform;

    private void Start()
    {
        _transform = GetComponent<Transform>();
    }

    public void SetPosition(Vector3 newPos)
    {
        _transform.position = newPos;
    }
}
