﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Objects.BallLogic
{
    public static class BallUtility
    {
        public static string BallColour(int ballSetIndex)
        {
            return ballSetIndex switch
            {
                50 => "Black",
                51 => "Cue",
                0 => "red",
                1 => "yellow",
                2 => "blue",
                3 => "orange",
                4 => "green",
                5 => "purple",
                6 => "magenta",
                7 => "brown",
                8 => "aqua",
                9 => "lime",
                _ => "Unset",
            };
        }
    }
}
