using Assets.GameManagement;
using Assets.Scripts.Objects.BallLogic;
using Assets.Scripts.Objects.MyGeneration;
using MLAPI;
using MLAPI.Messaging;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Objects
{
    public class BallHandler : NetworkBehaviour
    {
        private GameBalls gameBalls;
        private GameBalls gameBallInsts;
        private int numberOfPlayers;
        private bool shotIsInPlay;
        private int firstBallStruck;
        private List<GameObject> pocketedBalls;
        private List<GameObject> lostBalls;
        private List<GameObject> movingBalls;

        private List<string> lostBallFouls;

        public Dictionary<int, int> PlayerBallMapping;
        private int currentPlayer;

        public BallGenerator BallGenerator;

        public float ShotForce;
        

        // Events
        public static event Action<List<string>, bool, bool> ShotComplete;

        private void Awake()
        {
            // Events 
            GameManager.TurnStarted += BeginTurn;
            GameManager.StartGame += StartGame;

            
            pocketedBalls = new List<GameObject>();
            lostBalls = new List<GameObject>();
            movingBalls = new List<GameObject>();

            currentPlayer = 0;
            PlayerBallMapping = new Dictionary<int, int>();

        }

        public void BeginSession(Rules gameRules, int numPlayers)
        {
            if (IsHost)
            {
                Ball.BallMoving += OnBallMoving;
                Ball.BallStopped += OnBallStopped;
                Ball.BallSunk += OnBallSunk;
                Ball.BallOnFloor += OnBallHitFloor;
                CueBall.CueBallSunk += OnCueBallSunk;
                CueBall.CueBallStopped += OnCueBallStopped;

                RemoveBalls();
                PlayerBallMapping = new Dictionary<int, int>();
                numberOfPlayers = numPlayers;
                for (int i = 0; i < numPlayers; i++)
                {
                    PlayerBallMapping.Add(i, -1);
                }

                CreateBalls(gameRules);
                SpawnBalls();
            }
        }

        private void StartGame(int nextPlayer)
        {
            ResetPlayerBallMapping();
            StartGameClientRpc();
        }

        [ClientRpc]
        void StartGameClientRpc()
        {
            InputManager.OnShotReleased += Shot;
        }

        public void EndGame()
        {
            ResetPlayerBallMapping();

            // Server controls the calls from GameManager
            EndGameClientRpc();
        }

        [ClientRpc]
        void EndGameClientRpc()
        {
            InputManager.OnShotReleased -= Shot;
        }



        // Private methods ---------------------------------------------------------------------

        private void BeginTurn(Vector3 cueBallPos, Vector3 _, int player)
        {
            currentPlayer = player;
            firstBallStruck = -1;
            lostBalls.Clear();
            shotIsInPlay = false;
        }

        private void Shot(ShotInfo shot)
        {
            //Debug.Log("Shot");
            if (!IsHost)
            {
                //Debug.Log("Client sending shot");
                ClientShotServerRpc(shot);
                return;
            }

            if (!shotIsInPlay)
            {
                //scale the spin 0-1 based on the strength of the shot
                Vector3 scaledSpin = shot.Spin * ((shot.ForceScale / 100) - 1);
                if (Vector3.Distance(Vector3.right, shot.Spin.normalized) < 0.3)
                    scaledSpin *= 0;

                scaledSpin *= 2;
                gameBallInsts.CueBall.GetComponent<CueBall>().Move(shot.Direction, scaledSpin, ShotForce * (shot.ForceScale / 100));
                shotIsInPlay = true;
                movingBalls.Add(gameBallInsts.CueBall);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        void ClientShotServerRpc(ShotInfo si)
        {
            //Debug.Log("Server receiving Shot from Client");
            //Debug.Log("Shot with " + si.Direction.x + " x");
            Shot(si);

        }

        private void EndShot()
        {
            if (!IsHost) { return; }

            shotIsInPlay = false;

            Vector3 cueBallPos = gameBallInsts.CueBall.transform.position;

            if (pocketedBalls.Contains(gameBallInsts.CueBall) || lostBalls.Contains(gameBallInsts.CueBall))
            {
                cueBallPos = gameBalls.CueBallInitPosition;
            }

            ProcessLostBalls();

            List<int> ballpocketedIndexes = ProcessPockets();

            ShotProcessingInfo pockets = FoulLogic.ProcessShot(firstBallStruck, ballpocketedIndexes, ColouredBallsLeft(), PlayerBallMapping, currentPlayer);

            if (pockets.ColourToSet != 99)
            {
                UpdatePlayerBallMapping(currentPlayer, pockets.ColourToSet);
            }

            BallMapping.AutoUpdatePlayerBallMappings(PlayerBallMapping, ColouredBallsLeft());

            ShotComplete?.Invoke(pockets.fouls, pockets.LegalPocket, pockets.GoalBallPocketed);

        }

        private void OnBallMoving(GameObject ball)
        {
            if (!IsHost) { return; }

            movingBalls.Add(ball);
        }

        private void OnBallStopped(GameObject ball)
        {
            if (!IsHost) { return; }

            if (movingBalls.Contains(ball))
            {
                movingBalls.Remove(ball);
                CheckNoMovingBalls();
            }

        }

        private void OnCueBallStopped(List<GameObject> ballsStruck)
        {
            if (!IsHost) { return; }

            if (ballsStruck.Count > 0)
            {
                firstBallStruck = BallSetIndex(ballsStruck[0]);
            }

            // Cue ball has stopped
            if (movingBalls.Contains(gameBallInsts.CueBall))
            {
                movingBalls.Remove(gameBallInsts.CueBall);
                CheckNoMovingBalls();
            }

        }


        private void Pocketed(GameObject ball)
        {
            pocketedBalls.Add(ball);
        }

        private void Lost(GameObject ball)
        {
            if (movingBalls.Contains(ball))
            {
                movingBalls.Remove(ball);
            }

            lostBalls.Add(ball);

            CheckNoMovingBalls();
        }

        private void CheckNoMovingBalls()
        {
            if (movingBalls.Count == 0)
            {
                EndShot();
            }
        }

        private List<int> ProcessPockets()
        {
            // pocketedBalls to list of indexes
            List<int> ballpocketedIndexes = new List<int>();
            if (pocketedBalls.Count > 0)
            {
                ballpocketedIndexes.Add(BallSetIndex(pocketedBalls[0]));
            }

            CommitPocketedBalls();
           
            return ballpocketedIndexes;
        }

        private void CommitPocketedBalls()
        {
            foreach (GameObject ball in pocketedBalls)
            {
                for (int i = 0; i < gameBallInsts.TablePlayerBalls.Length; i++)
                {
                    if (gameBallInsts.TablePlayerBalls[i].Contains(ball))
                    {
                        //Debug.Log("Removing " + BallUtility.BallColour(BallSetIndex(ball)) + " from Table balls");
                        gameBallInsts.TablePlayerBalls[i].Remove(ball); 
                        gameBallInsts.PottedPlayerBalls[i].Add(ball);

                    }
                }

                if (ball.Equals(gameBallInsts.CueBall) || ball.Equals(gameBallInsts.GoalBall))
                {
                    ResetBall(ball);
                }
            }

            pocketedBalls.Clear();
        }


        private void ProcessLostBalls()
        {
            foreach (GameObject ball in lostBalls)
            {
                lostBallFouls.Add(BallUtility.BallColour(BallSetIndex(ball)) + " ball left table");
            }

            ResetBalls(lostBalls);

            lostBalls.Clear();
        }

        private void CreateBalls(Rules rules)
        {
            gameBalls = BallGenerator.GenerateBalls(rules, numberOfPlayers);
        }

        private void SpawnBalls()
        {
            gameBallInsts = new GameBalls(
                Instantiate(gameBalls.CueBall, gameBalls.CueBallInitPosition, Quaternion.identity),
                Instantiate(gameBalls.GoalBall, gameBalls.GoalBallInitPosition, Quaternion.identity),
                numberOfPlayers
                );

            gameBallInsts.CueBall.GetComponent<CueBall>().SetMaxShotPower(ShotForce);

            int k = 0;

            for (int i = 0; i < gameBalls.TablePlayerBalls[currentPlayer].Count; i++)
            {
                for (int j = 0; j < gameBalls.TablePlayerBalls.Length; j++)
                {
                    gameBallInsts.TablePlayerBalls[j].Add(Instantiate(gameBalls.TablePlayerBalls[j][i], gameBalls.BallInitPositions[k], Quaternion.identity));
                    k++;
                }
            }

            if (IsHost)
            {
                foreach (GameObject go in gameBallInsts.AllBalls())
                {
                    if (go.GetComponent<NetworkObject>())
                    {
                        go.GetComponent<NetworkObject>().Spawn();
                    }
                }
            }
        }

        private void ResetBalls(List<GameObject> balls)
        {
            int k = 0;

            foreach (GameObject ball in balls)
            {
                ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
                ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                if (gameBallInsts.CueBall.Equals(ball))
                {
                    ball.transform.position = gameBalls.CueBallInitPosition;
                }
                else
                {
                    ball.transform.position = gameBalls.BallInitPositions[k];
                }
                k++;
            }

        }

        private void ResetBall(GameObject ball)
        {
            ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            if (gameBallInsts.CueBall.Equals(ball))
            {
                ball.transform.position = gameBalls.CueBallInitPosition;
            }
            else if (gameBallInsts.GoalBall.Equals(ball))
            {
                ball.transform.position = gameBalls.GoalBallInitPosition;
            }
            else
            {
                ball.transform.position = gameBalls.BallInitPositions[0];
            }

        }

        public void ClearDown()
        {
            RemoveBalls();
            ResetPlayerBallMapping();
        }
        private void RemoveBalls()
        {
            if (gameBallInsts != null)
            {
                foreach (GameObject g in gameBallInsts.AllPlayersBalls())
                {
                    Destroy(g);
                }

                Destroy(gameBallInsts.GoalBall);
                Destroy(gameBallInsts.CueBall);
            }
        }

        public void OnBallSunk(GameObject ball)
        {
            Pocketed(ball);
        }

        public void OnBallHitFloor(GameObject ball)
        {
            Lost(ball);
        }

        public void OnCueBallSunk(bool inPocket)
        {
            if (inPocket)
            {
                Pocketed(gameBallInsts.CueBall);
            }
            else
            {
                Lost(gameBallInsts.CueBall);
            }
        }

        public Vector3 CueBallCurrentPosition()
        {
            return gameBallInsts.CueBall.transform.position;
        }

        public Vector3 AverageBallPosition(int playerMapping = -1)
        {
            if (!IsHost)
            {
                return new Vector3(0, 0, 0);
            }

            return gameBallInsts.TableBallsAveragePosition(playerMapping);
        }

        public bool PlayerCanHaveExtraShot(int player)
        {
            if (PlayerBallMapping[player] == 50)
            {
                return false;
            }

            return true;
        }

        public List<int> ColouredBallsLeft()
        {
            List<int> numberBallsOnTable = new List<int>();

            for (int i=0;i< gameBallInsts.TablePlayerBalls.Length; i++)
            {
                numberBallsOnTable.Add(gameBallInsts.TablePlayerBalls[i].Count);
            }

            return numberBallsOnTable;
        }

        private int BallSetIndex(GameObject ball)
        {
            for (int i = 0; i < gameBallInsts.TablePlayerBalls.Length; i++)
            {
                if (gameBallInsts.TablePlayerBalls[i].Contains(ball))
                {
                    return i;
                }
            }

            for (int i = 0; i < gameBallInsts.PottedPlayerBalls.Length; i++)
            {
                if (gameBallInsts.PottedPlayerBalls[i].Contains(ball))
                {
                    return i;
                }
            }

            if (gameBallInsts.GoalBall.Equals(ball))
            {
                return 50;
            }

            if (gameBallInsts.CueBall.Equals(ball))
            {
                return 51;
            }

            return -1;

        }

        public string PlayerColour(int player)
        {
            var playerMapping = PlayerBallMapping[player];

            if (playerMapping != -1)
            {
                if (ColouredBallsLeft()[playerMapping] == 0)
                {
                    playerMapping = 50;
                }
            }

            return BallUtility.BallColour(playerMapping);
        }

        private void ResetPlayerBallMapping()
        {
            for (int i = 0; i < numberOfPlayers; i++)
            {
                PlayerBallMapping[i] = -1;
            }
        }

        private void UpdatePlayerBallMapping(int playerId, int mapping)
        {
            if (!IsHost)
            {
                return;
            }
            PlayerBallMapping[playerId] = mapping;

        }

        public string PlayerMappingDisplay()
        {
            string mappings = "";
            for (int i = 0; i < numberOfPlayers; i++)
            {
                mappings += "Player " + (i + 1) + " = " + PlayerBallMapping[i] + " ";
            }

            return mappings;
        }

    }
}
