﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Objects.MyGeneration
{
    public class GameBalls
    {
        public GameObject CueBall;
        public List<GameObject>[] TablePlayerBalls;
        public List<GameObject>[] PottedPlayerBalls;
        public GameObject GoalBall;
        public Vector3 CueBallInitPosition;
        public Vector3 GoalBallInitPosition;
        public List<Vector3> BallInitPositions;

        private readonly int numberPlayers;


        public GameBalls(GameObject cueBall, GameObject goalBall, int numPlayers)
        {
            numberPlayers = numPlayers;
            CueBall = cueBall;
            GoalBall = goalBall;

            TablePlayerBalls = new List<GameObject>[numberPlayers];
            for (int i = 0; i < numberPlayers; i++) { TablePlayerBalls[i] = new List<GameObject>(); }

            PottedPlayerBalls = new List<GameObject>[numberPlayers];
            for (int i = 0; i < numberPlayers; i++) { PottedPlayerBalls[i] = new List<GameObject>(); }
        }

        public List<GameObject> AllPlayersBalls()
        {
            List<GameObject> balls = new List<GameObject>();
            foreach (List<GameObject> player in TablePlayerBalls)
            {
                balls.AddRange(player);
            }
            foreach (List<GameObject> player in PottedPlayerBalls)
            {
                balls.AddRange(player);
            }

            return balls;
        }

        public List<GameObject> AllTableBalls()
        {
            List<GameObject> balls = new List<GameObject>();
            foreach (List<GameObject> player in TablePlayerBalls)
            {
                balls.AddRange(player);
            }

            return balls;
        }

        public List<GameObject> AllBalls()
        {
            List<GameObject> balls = new List<GameObject>();
            foreach (List<GameObject> player in TablePlayerBalls)
            {
                balls.AddRange(player);
            }
            foreach (List<GameObject> player in PottedPlayerBalls)
            {
                balls.AddRange(player);
            }

            balls.Add(CueBall);
            balls.Add(GoalBall);

            return balls;
        }

        public Vector3 TableBallsAveragePosition()
        {
            int numBalls = 1;
            Vector3 avgPos = new Vector3();

            foreach (List<GameObject> balls in TablePlayerBalls)
            {          
                if (balls.Count != 0)
                {
                    foreach (GameObject ball in balls)
                    {
                        avgPos += ball.transform.position;
                        numBalls += 1;
                    }
                }
            }

            return avgPos / numBalls;
        }

        public Vector3 TableBallsAveragePosition(int ballSet)
        {
            // if unset, return the average position of all balls
            if (ballSet == -1)
            {
                return TableBallsAveragePosition();
            }
            if (ballSet == 50)
            {
                return GoalBall.transform.position;
            }

            int numBalls = 0;
            Vector3 avgPos = new Vector3();

            if (TablePlayerBalls[ballSet].Count != 0)
            {
                foreach (GameObject go in TablePlayerBalls[ballSet])
                {
                    avgPos += go.transform.position;
                    numBalls += 1;
                }
            }
            else
            {
                numBalls = 1;
                avgPos = GoalBall.transform.position;
            }

            return avgPos / numBalls;
        }
    

        public void SetPlayerBalls(int player, GameObject ball, int quantity)
        {
            for (int i=0;i< quantity; i++)
            {
                TablePlayerBalls[player].Add(ball);
            }
        }

        public void ResetPotted()
        {
            for (int i=0;i< numberPlayers;i++)
            {
                foreach (GameObject ball in PottedPlayerBalls[i])
                {
                    TablePlayerBalls[i].Add(ball);
                }

                PottedPlayerBalls[i].Clear();
            }
        }

    }
}
