﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Objects.BallLogic
{
    public static class FoulLogic
    {
        public static List<string> CalculateBallStruckFouls(int ballIndex, Dictionary<int,int> playerBallMapping, int playerBallsLeftOnTable, int currentPlayer)
        {
            List<string> ballStruckFouls = new List<string>();

            // if balls were struck
            if (ballIndex != -1)
            {
                // If player is unset
                if (playerBallMapping[currentPlayer] == -1)
                {
                    // if it's the black ball
                    if (ballIndex == 50)
                    {
                        ballStruckFouls.Add("Black ball struck early");
                    }
                    // if it's another player's colour
                    else if (playerBallMapping.ContainsValue(ballIndex))
                    {
                        ballStruckFouls.Add("Incorrect colour struck - " + BallUtility.BallColour(ballIndex));
                    }

                }
                else // if player is set
                {
                    // if the ball isn't player's colour
                    if (playerBallMapping[currentPlayer] != ballIndex)
                    {
                        // if it's not the goal ball
                        if (ballIndex != 50)
                        {
                            ballStruckFouls.Add("Incorrect colour struck - " + BallUtility.BallColour(ballIndex));
                        }
                        else
                        {
                            // if player still has coloured balls to pot
                            if (playerBallsLeftOnTable != 0)
                            {
                                ballStruckFouls.Add("Incorrect colour struck - " + BallUtility.BallColour(ballIndex));
                            }
                        }
                    }
                }
            }
            else
            {
                ballStruckFouls.Add("No balls struck");
            }

            return ballStruckFouls;
        }

        public static ShotProcessingInfo ProcessShot(int firstBallStruck, List<int> pocketedBalls, List<int> ballsLeftOnTable, Dictionary<int,int> playerBallMapping, int currentPlayer)
        {
            ShotProcessingInfo output = new ShotProcessingInfo();

            int playerBallsLeftOnTable = -1;

            if (playerBallMapping[currentPlayer] != -1)
            {
                playerBallsLeftOnTable = ballsLeftOnTable[playerBallMapping[currentPlayer]];
            }

            output.fouls.AddRange(CalculateBallStruckFouls(firstBallStruck, playerBallMapping, playerBallsLeftOnTable, currentPlayer));

            foreach (int ballSetIndex in pocketedBalls)
            {
                // If player is unset at start of turn
                if (playerBallMapping[currentPlayer] == -1)
                {
                    // if it's the black ball
                    if (ballSetIndex == 50)
                    {
                        // if the player wasn't set during the turn
                        // or if the player was set, but still has balls remaining
                        if (output.ColourToSet == 99 || ballsLeftOnTable[output.ColourToSet] != 0)
                        {
                            output.fouls.Add("Black ball pocketed early");
                        }
                        else
                        {
                            output.LegalPocket = true;
                        }

                        output.GoalBallPocketed = true;
                    }
                    else if (ballSetIndex == 51)
                    {
                        output.fouls.Add("Cue ball pocketed");
                    }
                    // if it's another player's colour
                    else if (playerBallMapping.ContainsValue(ballSetIndex))
                    {
                        output.fouls.Add("Incorrect colour pocketed - " + BallUtility.BallColour(ballSetIndex));
                    }
                    //  if it's nobody's colour, and we're not already setting a colour, take it
                    else if (output.ColourToSet == 99)
                    {
                        output.LegalPocket = true;
                        output.ColourToSet = ballSetIndex;
                    }
                }
                else // if player is set
                {
                    // if the ball isn't player's colour
                    if (playerBallMapping[currentPlayer] != ballSetIndex)
                    {
                        // if it's the black ball 
                        if (ballSetIndex == 50)
                        {
                            // and the current player has remaining coloured balls
                            if (ballsLeftOnTable[playerBallMapping[currentPlayer]] != 0)
                            {
                                output.fouls.Add("Incorrect colour pocketed - " + BallUtility.BallColour(ballSetIndex));
                            }
                            else
                            {
                                output.LegalPocket = true;
                            }

                            output.GoalBallPocketed = true;
                        }
                        else // if it's just the wrong colour
                        {
                            output.fouls.Add("Incorrect colour pocketed - " + BallUtility.BallColour(ballSetIndex));
                        }
                    
                    }
                    else
                    {
                        output.LegalPocket = true;
                    }
                }


            }

            return output;
        }



    }

    public class ShotProcessingInfo
    {
        public bool LegalPocket;
        public bool GoalBallPocketed;
        public int ColourToSet;
        public List<string> fouls;

        public ShotProcessingInfo()
        {
            LegalPocket = false;
            GoalBallPocketed = false;
            ColourToSet = 99;
            fouls = new List<string>();
        }
    }
}
