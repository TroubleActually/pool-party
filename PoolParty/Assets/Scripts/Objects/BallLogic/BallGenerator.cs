using Assets.Scripts.Objects.MyGeneration;
using Assets.GameManagement;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject CueBall;
    public GameObject GoalBall;
    public List<GameObject> PlayerBallPrefabs;
    public float BallDiameter;
    public float BallSpawnSpacing;
    public float BallSpawnHeight;

    public int MinimumBallsPerPlayer;

    public GameBalls GenerateBalls(Rules rules, int players)
    {
        GameBalls balls = new GameBalls(CueBall, GoalBall, players);

        switch (rules)
        {
            case Rules.UkPool:

                var ballsPerPlayer = 14 / players;

                if (ballsPerPlayer < MinimumBallsPerPlayer)
                    ballsPerPlayer = MinimumBallsPerPlayer;
                
                for (int i=0; i<players; i++)
                {
                    balls.SetPlayerBalls(i, PlayerBallPrefabs[i], ballsPerPlayer);    
                }

                balls.CueBallInitPosition = new Vector3(5f, 9.8f, 3.675f);
                balls.BallInitPositions = TrianglePositions(ballsPerPlayer * players, new Vector3(5.0f, 9.8f + BallSpawnHeight, 14.5f));
                
                // Take the 5th ball's position for the black
                balls.GoalBallInitPosition = balls.BallInitPositions[4];
                balls.BallInitPositions.RemoveAt(4);
                

                return balls;
                
            case Rules.JustTheBlack:

                balls.CueBallInitPosition = new Vector3(5f, 9.8f, 3.675f);
                balls.GoalBallInitPosition = new Vector3(5.0f, 9.8f + BallSpawnHeight, 13.9f);
                balls.BallInitPositions = new List<Vector3>() { balls.GoalBallInitPosition };
                return balls;

            default:
                return balls;
        }
    }

    private List<Vector3> TrianglePositions(int numBalls, Vector3 startPos)
    {
        List<Vector3> triangle = new List<Vector3>();

        int ballsAdded = 0;
        int ballsInRow = 1;
        float xIncrement = 0.5f;
        float zIncrement = 0.9f;

        float xPos = startPos.x;
        float yPos = startPos.y;
        float zPos = startPos.z;
        bool oddRow = true;

        float ballSpacing = BallDiameter + (BallSpawnSpacing/2);

        triangle.Add(startPos);

        xPos += ballSpacing;

        while (ballsAdded < numBalls)
        {
            oddRow = !oddRow;
            ballsInRow += 1;

            // Snake down the pyramid
            if (oddRow) { xPos += xIncrement * ballSpacing; }
            else { xPos -= xIncrement * ballSpacing; }

            zPos += zIncrement * ballSpacing;

            // if there are fewer balls left to add than the ideal triangular row
            if (ballsInRow > (numBalls-ballsAdded))
            {
                // centre the final row by shifting it in by half of the remaining space
                int shift = (ballsInRow - (numBalls - ballsAdded)) / 2;

                if (oddRow) { xPos += shift * ballSpacing; }
                else { xPos -= shift * ballSpacing; }
            }

            for (int i = 0; i < ballsInRow; i++)
            {
                triangle.Add(new Vector3(xPos, yPos, zPos));
 
                ballsAdded += 1;

                if (oddRow) { xPos += ballSpacing; }
                else { xPos -= ballSpacing; }
            }

        }

        return triangle;

    }



    
}
