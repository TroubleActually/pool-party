﻿using System.Collections.Generic;

namespace Assets.Scripts.Objects.BallLogic
{
    public static class BallMapping
    {
        public static void AutoUpdatePlayerBallMappings(Dictionary<int, int> currentMapping, List<int> ballsLeftOnTable)
        {

            List<int> playersUnset = new List<int>();

            if (currentMapping.ContainsValue(-1))
            {
                foreach (int player in currentMapping.Keys)
                {
                    if (currentMapping[player] == -1)
                    {
                        playersUnset.Add(player);
                    }
                }

                // Add last player standing to last mapping standing
                if (playersUnset.Count == 1)
                {
                    for (int i = 0; i < currentMapping.Count; i++)
                    {
                        // find the ball index between 0 and numPlayers-1 which is not present in the ball mappings
                        // and has balls left on the table
                        if (!currentMapping.ContainsValue(i) && ballsLeftOnTable[i] != 0)
                        {
                            currentMapping[playersUnset[0]] = i;
                        }
                    }

                }
            }

        }
    }
}
