﻿using MLAPI;
using MLAPI.Messaging;
using System;
using UnityEngine;

namespace Assets.Scripts.Objects
{
    public class Ball : NetworkBehaviour
    {
        private Rigidbody ball;
        private AudioSource audioSrc;
        public AudioClip BallClack;
        public AudioClip RailHit;
        public AudioClip BallInTable;
        private bool latch;
        public float moveThreshold;

        //Events
        public static event Action<GameObject> BallSunk;
        public static event Action<GameObject> BallOnFloor;
        public static event Action<GameObject> BallMoving;
        public static event Action<GameObject> BallStopped;

        private void Start()
        {
            ball = GetComponent<Rigidbody>();
            audioSrc = GetComponent<AudioSource>();
            ball.maxAngularVelocity = 50;
            latch = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Hole>())
            {
                BallSunk?.Invoke(gameObject);
                audioSrc.PlayOneShot(BallInTable);
            }
            else if (other.GetComponent<Floor>())
            {
                BallOnFloor?.Invoke(gameObject);
            }

        }

        [ClientRpc]
        void PlayBallClackClientRpc(float magnitude)
        {
            audioSrc.PlayOneShot(BallClack, magnitude);
        }

        [ClientRpc]
        void PlayRailHitClientRpc(float magnitude)
        {
            audioSrc.PlayOneShot(RailHit, magnitude);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (IsHost)
            {
                if (collision.gameObject.GetComponent<Ball>())
                {
                    if (collision.gameObject.GetInstanceID() > gameObject.GetInstanceID())
                    {
                        PlayBallClackClientRpc(collision.relativeVelocity.magnitude / 35);
                    }
                }
                else if (collision.gameObject.GetComponent<Rail>())
                {
                    PlayRailHitClientRpc(collision.relativeVelocity.magnitude / 35);
                }
            }
        }

        private void Update()
        {
            if (ball.angularVelocity.magnitude > moveThreshold && ball.velocity.magnitude > moveThreshold)
            {
                if (!latch)
                {
                    latch = true;
                    BallMoving?.Invoke(gameObject);
                }
            }
            else
            {
                if (latch)
                {
                    ball.velocity = Vector3.zero;
                    ball.angularVelocity = Vector3.zero;
                    BallStopped?.Invoke(gameObject);
                    latch = false;
                }
            }

        }
    }
}
