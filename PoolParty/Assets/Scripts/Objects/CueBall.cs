using UnityEngine;
using System;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;

namespace Assets.Scripts.Objects
{
    public class CueBall : NetworkBehaviour
    {
        public float spin;

        private Rigidbody ball;

        private AudioSource audioSrc;
        public AudioClip BallClack;
        public AudioClip RailHit;
        public AudioClip CueHit;
        public AudioClip BallInTable;
        private float maxShotPower;

        private bool isMoving;
        private bool isOnTable;
        private bool isInPocket;
        private bool moveLatch;
        public float moveThreshold;
        private List<GameObject> ballsStruck;

        //Events
        public static event Action<bool> CueBallSunk;
        public static event Action<List<GameObject>> CueBallStopped;

        private void Start()
        {
            ball = GetComponent<Rigidbody>();
            audioSrc = GetComponent<AudioSource>();
            ball.maxAngularVelocity = 50;

            isMoving = false;
            isOnTable = false;
            isInPocket = false;
            moveLatch = false;

            ballsStruck = new List<GameObject>();
            
        }

        internal void SetMaxShotPower(float shotForce)
        {
            maxShotPower = shotForce;
        }

        public void Move(Vector3 v, Vector3 s, float f)
        {
            audioSrc.PlayOneShot(CueHit, f / maxShotPower);
            Vector3 yNormDirection = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 direction = yNormDirection - v;
            isMoving = true;
            isInPocket = false;
            ballsStruck.Clear();
            ball.AddForce(direction * f, ForceMode.Impulse);
            ball.AddTorque(spin * -s, ForceMode.Impulse);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Hole>())
            {
                CueBallSunk?.Invoke(true);
                isInPocket = true;
                audioSrc.PlayOneShot(BallInTable);
            }
            else if (other.GetComponent<Floor>())
            {
                CueBallSunk?.Invoke(false);
            }
        }
               
        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.GetComponentInChildren<TabletopSurface>())
            {
                isOnTable = false;
            }

        }

        [ClientRpc]
        void PlayBallClackClientRpc(float magnitude)
        {
            audioSrc.PlayOneShot(BallClack,magnitude);
        }

        [ClientRpc]
        void PlayRailHitClientRpc(float magnitude)
        {
            audioSrc.PlayOneShot(RailHit, magnitude);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (IsHost)
            {
                if (collision.gameObject.GetComponent<Ball>())
                {
                    ballsStruck.Add(collision.gameObject);
                    PlayBallClackClientRpc(collision.relativeVelocity.magnitude / 35);
                }
                else if (collision.gameObject.GetComponent<Rail>())
                {
                    PlayRailHitClientRpc(collision.relativeVelocity.magnitude / 35);
                }
            }

            if (collision.gameObject.GetComponentInChildren<TabletopSurface>())
            {
                isOnTable = true;
            }

        }

        private void OnCollisionStay(Collision collision)
        {
            if (collision.gameObject.GetComponentInChildren<TabletopSurface>())
            {
                isOnTable = true;
                
            }
        }


        private void FixedUpdate()
        {
            if (isMoving)
            {
                if (ball.angularVelocity.magnitude > moveThreshold && ball.velocity.magnitude > moveThreshold)
                {
                    moveLatch = true;
                }
                else
                {
                    if (moveLatch && (isOnTable || isInPocket))
                    {
                        ball.velocity = Vector3.zero;
                        ball.angularVelocity = Vector3.zero;
                        CueBallStopped?.Invoke(ballsStruck);
                        ballsStruck.Clear();
                        isMoving = false;
                        moveLatch = false;
                    }
                }

            }
        }
    }
}
