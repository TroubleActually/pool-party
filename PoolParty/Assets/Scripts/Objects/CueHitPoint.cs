using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueHitPoint : MonoBehaviour
{
    public Material ActiveMaterial;
    public Material DormantMaterial;

    private Renderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    public void SetColour(bool active)
    {
        if (active)
        {
            _renderer.material = ActiveMaterial;
        }
        else
        {
            _renderer.material = DormantMaterial; 
        }
    }
}
