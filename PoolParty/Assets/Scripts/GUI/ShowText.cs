using Assets.GameManagement;
using MLAPI;
using UnityEngine;
using UnityEngine.UI;

public class ShowText : NetworkBehaviour
{
    public string textValue;
    public Text textElement;
    public int Position;

    private int turnCounter;
    private int targetTurn;

    private void Awake()
    {
        GameManager.StartGame += ClearText;
        GameManager.DisplayText += ChangeText;
        GameManager.TurnStarted += TurnChange;
        GameManager.DisplayTextForTurns += ShowTextForTurns;
    }

    public void ChangeText(string text, int position)
    {
        if (position == Position)
        {
            textElement.text = text;
        }
    }

    public void ShowTextForTurns(string text, int turnsInclusive, int position)
    {
        if (position == Position)
        {
            textElement.text = text;
            turnCounter = 0;
            targetTurn = turnsInclusive;
        }
    }

    private void ClearText()
    {
        textElement.text = "";
    }
    private void ClearText(int i)
    {
        textElement.text = "";
    }

    // Deal with the GameManager StartTurn event
    private void TurnChange(Vector3 cuePos, Vector3 _, int player)
    {
        turnCounter += 1;

        if (turnCounter == targetTurn)
        {
            ClearText();
        }
    }
}
