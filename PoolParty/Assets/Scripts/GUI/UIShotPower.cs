using Assets.GameManagement;
using UnityEngine;
using UnityEngine.UI;

public class UIShotPower : MonoBehaviour
{
    public string textValue;
    public Text textElement;
    public int Position;

    private Slider slider;
    private bool powerIncreasing;
    private int power;

    private void Awake()
    {
        GameManager.TurnStarted += OnNextTurn;
        InputManager.OnShotHeld += Show;
        InputManager.OnShotReleased += Cap;

        slider = GetComponentInChildren<Slider>();
    }

    // Deal with input event
    public void Show()
    {
        if (power == 0)
            powerIncreasing = true;
    }

    private void Cap(ShotInfo s)
    {
        if (powerIncreasing == true)
            powerIncreasing = false;
    }

    public void Update()
    {
        if (powerIncreasing && power < 100)
        {
            power += 1;
            RefreshPower(); 
        }
    }

    private void OnNextTurn(Vector3 v, Vector3 _, int p)
    {
        Clear();
    }

    private void RefreshPower()
    {
        textElement.text = power.ToString();
        slider.value = power;
    }

    private void Clear()
    {
        power = 0;

        textElement.text = "";
        slider.value = power;
    }

}
